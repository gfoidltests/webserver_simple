﻿#define TEST

using System;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Mono.Unix;
using Mono.Unix.Native;

namespace WebServer_Simple
{
	static class Program
	{
		static void Main(string[] args)
		{
			using (WebServer ws = new WebServer(SendResponseAsync, Properties.Settings.Default.ServerUrl))
			{
				ws.Started 		   += (s, e) => Console.WriteLine("Webserver running...");
				ws.Stopped 		   += (s, e) => Console.WriteLine("Webserver stopped.");
				ws.RequestReceived += (s, e) =>
				{
					Console.ForegroundColor = ConsoleColor.DarkGreen;
					Console.WriteLine("received request at {0} from {1} on thread {2}", e.Time.ToLongTimeString(), e.Client, e.ThreadId);
					Console.ResetColor();
				};

				ws.Run();
#if TEST
				Test();
#endif
				Console.Write("A simple webserver. ");
				if (Type.GetType("Mono.Runtime") != null)
				{
					Console.WriteLine("Press Ctrl+C to quit");
					// Quelle: https://github.com/NancyFx/Nancy/wiki/Hosting-Nancy-with-Nginx-on-Ubuntu
					// on mono, processes will usually run as daemons - this allows you to listen
					// for termination signals (ctrl+c, shutdown, etc) and finalize correctly
					UnixSignal.WaitAny(new[] {
						new UnixSignal(Signum.SIGINT),
						new UnixSignal(Signum.SIGTERM),
						new UnixSignal(Signum.SIGQUIT),
						new UnixSignal(Signum.SIGHUP)
					});
				}
				else
				{
					Console.WriteLine("Press a key to quit.");
					Console.ReadKey();
				}

				Console.WriteLine("Stopping");
				ws.Stop();
			}
		}
		//---------------------------------------------------------------------
		public static async Task<string> SendResponseAsync(HttpListenerRequest request)
		{
			// Lange Operation simulieren:
			await Task.Delay(2500).ConfigureAwait(false);
			return $"Service running. Server time: {DateTime.Now}\tT-ID: {Thread.CurrentThread.ManagedThreadId}";
		}
		//---------------------------------------------------------------------
		[Conditional("TEST")]
		private static void Test()
		{
			Action doIt = () =>
			{
				using (WebClient wc = new WebClient())
				{
					string result = wc.DownloadString(Properties.Settings.Default.ServerUrl);
				}
			};

			Parallel.Invoke(doIt, doIt);
		}
	}
}