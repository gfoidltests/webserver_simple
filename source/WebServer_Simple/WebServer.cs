﻿using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace System.Net
{
	public class WebServer : IDisposable
	{
		private readonly HttpListener 					   		 _listener = new HttpListener();
		private readonly Func<HttpListenerRequest, Task<string>> _responderMethod;
		//---------------------------------------------------------------------
		public WebServer(Func<HttpListenerRequest, Task<string>> method, params string[] prefixes)
		{
			if (!HttpListener.IsSupported) throw new NotSupportedException("Needs Windows XP SP2, Server 2003 or later.");

			// URI prefixes are required, for example 
			// "http://localhost:8080/index/".
			if (prefixes == null || prefixes.Length == 0) throw new ArgumentException("prefixes");

			// A responder method is required
			if (method == null) throw new ArgumentException("method");

			foreach (string s in prefixes)
				_listener.Prefixes.Add(s);

			_responderMethod = method;
			_listener.Start();
		}
		//---------------------------------------------------------------------
		public void Run()
		{
			this.ThrowIfDisposed();

			Task.Factory.StartNew(async () =>
			{
				try
				{
					while (_listener.IsListening)
					{
						var ctx = await _listener.GetContextAsync().ConfigureAwait(false);
						this.OnRequestReceived(ctx);
						Task.Run(() => this.HandleRequestAsync(ctx));
					}
				}
				catch { } // suppress any exceptions
			}, TaskCreationOptions.LongRunning);

			this.OnStarted();
		}
		//---------------------------------------------------------------------
		private async Task HandleRequestAsync(HttpListenerContext ctx)
		{
			try
			{
				string rstr = await _responderMethod(ctx.Request).ConfigureAwait(false);
				byte[] buf = Encoding.UTF8.GetBytes(rstr);
				ctx.Response.ContentLength64 = buf.Length;
				ctx.Response.OutputStream.Write(buf, 0, buf.Length);
			}
			finally
			{
				// always close the stream
				ctx.Response.OutputStream.Close();
			}
		}
		//---------------------------------------------------------------------
		public void Stop()
		{
			this.ThrowIfDisposed();

			_listener.Stop();

			this.OnStopped();
		}
		//---------------------------------------------------------------------
		#region Events
		public event EventHandler<EventArgs> Started;
		public event EventHandler<EventArgs> Stopped;
		public event EventHandler<RequestReceivedEventArgs> RequestReceived;
		//---------------------------------------------------------------------
		private void OnStarted()
		{
			this.Started?.Invoke(this, EventArgs.Empty);
		}
		//---------------------------------------------------------------------
		private void OnStopped()
		{
			this.Stopped?.Invoke(this, EventArgs.Empty);
		}
		//---------------------------------------------------------------------
		private void OnRequestReceived(HttpListenerContext context)
		{
			this.RequestReceived?.Invoke(this, new RequestReceivedEventArgs(context));
		}
		#endregion
		//---------------------------------------------------------------------
		#region IDisposable Members
		private bool _isDisposed = false;
		//---------------------------------------------------------------------
		[DebuggerStepThrough]
		protected void ThrowIfDisposed()
		{
			if (_isDisposed) throw new ObjectDisposedException(this.ToString());
		}
		//---------------------------------------------------------------------
		protected virtual void Dispose(bool disposing)
		{
			if (!_isDisposed)
			{
				if (disposing)
				{
					_listener.Stop();
					_listener.Close();
				}

				_isDisposed = true;
			}
		}
		//---------------------------------------------------------------------
		[DebuggerStepThrough]
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}
		#endregion
	}
	//-------------------------------------------------------------------------
	public class RequestReceivedEventArgs : EventArgs
	{
		public IPEndPoint Client { get; set; }
		public DateTime Time 	 { get; set; }
		public int ThreadId 	 { get; set; }
		//---------------------------------------------------------------------
		public RequestReceivedEventArgs(HttpListenerContext context)
		{
			this.Client   = context.Request.RemoteEndPoint;
			this.Time 	  = DateTime.Now;
			this.ThreadId = Thread.CurrentThread.ManagedThreadId;
		}
	}
}